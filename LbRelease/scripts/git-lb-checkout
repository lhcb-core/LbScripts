#!/bin/bash -e

name='git lb-checkout'

function print_usage() {
  echo "usage: $name [options] <branch> <path>"
}

function print_help() {
print_usage
cat <<EOF

    -c, --commit          commit immediately after checkout (default)
    --no-commit           do not commit after checkout

<branch>: name of the branch/tag/commit used to get data from (e.g. LHCb/master)
<path>: name of a file or directory to checkout from the specified branch
EOF
exit 0
}

# parse options
opts=$(getopt -n "$name" -o ch -l commit,no-commit -- "$@")
[ $? != 0 ] && exit 1
eval set -- $opts

# defaults
do_commit=1

while true ; do
  case $1 in
    -c|--commit) do_commit=1; shift;;
    --no-commit) do_commit=; shift;;
    -h|--help) print_help ;;
    --) shift; break ;;
  esac
done

if [ "$#" -ne 2 ] ; then
  echo "error: wrong number of argument (see $name -h)"
  print_usage
  exit 1
fi

commit="$1"
if git rev-parse --verify "$commit" > /dev/null 2>&1 ; then
  # Good commit name/id
  true
else
  echo "error: invalid reference: $commit"
  echo "did you forget to call 'git lb-use'?"
  exit 1
fi

# handle the case of a commit id instead of a branch name
branch_name=$(git branch -r --contains "$commit" | sed -e 's/^[[:space:]]*//' | head -1)
if [ -z "$branch_name" ] ; then
  # try with a tag
  branch_name=$(git tag --contains "$commit" | sed -e 's/^[[:space:]]*//' | head -1)
fi
remote="${branch_name%%/*}"
if [ -z "$remote" ] ; then
  echo "error: cannot find the remote repository containing ${commit}"
  exit 1
fi

path="$2"
# strip trailing '/'
path=${path%/}

git checkout "$commit" -- "$path"

# get the qualified path (if the checkout was called from a subdirectory)
path="$(git rev-parse --show-prefix)${path}"

config_file="$(git rev-parse --show-toplevel)/.git-lb-checkout"
if [ ! -e "${config_file}" ] ; then
  # handle migration from old style: move old entries to the new config file
  git config --get-regexp "lb-checkout\.*" | while read l ; do
    git config -f "${config_file}" $l
  done
  for name in $(git config --get-regexp "lb-checkout\.*" | awk '{print $1}') ; do
    git config --unset $name
  done
fi

git config -f "${config_file}" "lb-checkout.${remote}.${path}.base" "$(git rev-parse HEAD)"
git config -f "${config_file}" "lb-checkout.${remote}.${path}.imported" "$(git rev-parse $commit)"
git add "${config_file}"

if [ $do_commit ] ; then
  git commit -m "added ${path} from ${remote} (${commit})"
fi

top_cmake="$(git rev-parse --show-toplevel)/CMakeLists.txt"
test -e "${top_cmake}" && touch "${top_cmake}"
